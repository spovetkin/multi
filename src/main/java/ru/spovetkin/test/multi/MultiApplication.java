package ru.spovetkin.test.multi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.task.TaskExecutor;

import javax.annotation.PostConstruct;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.IntStream;

@SpringBootApplication
public class MultiApplication {

    @Autowired
    private TaskExecutor myTaskExecutor;


    public static void main(String[] args) {
        SpringApplication.run(MultiApplication.class, args);
    }

    @PostConstruct
    private void postConstruct() {
        CompletableFuture[] futures = IntStream.range(1, 5).mapToObj(i -> createFuture(i, true))
                .toArray(CompletableFuture[]::new);
        CompletableFuture<Void> run = CompletableFuture.allOf(futures);
        try {
            run.get();
        } catch (InterruptedException | ExecutionException e) {
            System.out.println("Error: " + e.getMessage());
        }
        System.out.println("END");

    }


    private CompletableFuture<Void> createFuture(int num, boolean withRuntimeException) {
        if (withRuntimeException && num == 1) {
            return CompletableFuture.runAsync(() -> {
                throw new RuntimeException("runtime exception");
            }, myTaskExecutor);
        }

        return CompletableFuture.runAsync(() -> {
            IntStream.range(1, 1000000).forEach(i -> System.out.println("Поток " + num + "  " + i));
        }, myTaskExecutor);
    }

}
